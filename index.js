const axios = require('axios')
const mongoose = require('mongoose')
const bodyparser = require('body-parser')
const cors = require('cors')
const Item = require('./models/item.model')
var cron = require('node-cron')
const itemRouter = require('./routes/items.routes')
const keyRouter = require('./routes/keys.routes')
const express = require('express')
const app = express()
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({ extended: true }))
app.use(cors())
app.use('/items', itemRouter)
app.use('/keys', keyRouter)
const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log('server running..')
})

mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose.set('useUnifiedTopology', true)
mongoose.connect(
  'mongodb+srv://safwen:01161590@cluster0-jwcqg.mongodb.net/gaming?retryWrites=true&w=majority',
)
mongoose.connection.on('connected', () => {
  console.log('Connected')
})
mongoose.connection.on('error', (err) =>
  console.log('Connection failed with - ', err),
)

const deleteMany = async () => {
  let result = await Item.deleteMany({})
  console.log(result)
}
//deleteMany();

setInterval(async () => {
  deleteMany()

  axios
    .get('https://tarkov-market.com/api/v1/items/all', {
      headers: { 'x-api-key': 'zrt4fMmXVww0bHrk' },
    })
    .then((res) => {
      //console.log(res);
      let items = res.data
      items.forEach(async (element) => {
        console.log(element)
        let newItem = Item({
          name: element.name,
          shortName: element.shortName,
          price: element.price,
          avg24hPrice: element.avg24hPrice,
          avg7daysPrice: element.avg7daysPrice,
          traderName: element.traderName,
          traderPrice: element.traderPrice,
          traderPriceCur: element.traderPriceCur,
          updated: element.updated,
          slots: element.slots,
          diff24h: element.diff24h,
          diff7days: element.diff7days,
          icon: element.icon,
          img: element.img,
          imgBig: element.imgBig,
          bsgId: element.bsgId,
          reference: element.reference,
        })
        await newItem.save()
      })
    })
    .catch((er) => {
      console.log(er)
    })
}, 10 * 60 * 60 * 1000)
