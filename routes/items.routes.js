const express = require('express')
const itemController = require('../controllers/items.controller')
const verifKey = require('../utils/verifKey')
const router = express.Router()

router.get('/', verifKey, itemController.getAllItems)
router.get('/find/:id', itemController.findById)
router.get('/find/:itemName', itemController.findByName)
router.get('/grapitems', itemController.grapItems) // only for testing
module.exports = router
