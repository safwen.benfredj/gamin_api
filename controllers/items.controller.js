const Item = require("../models/item.model");
const axios = require("axios")
module.exports.grapItems = async (req, res) => {

  try {
    const response = await axios.get("https://tarkov-market.com/api/v1/items/all", {
      headers: { "x-api-key": "zrt4fMmXVww0bHrk" },
    })
    let items = response.data;
    items.forEach(async (element) => {
      let newItem = Item({
        name: element.name,
        shortName: element.shortName,
        price: element.price,
        avg24hPrice: element.avg24hPrice,
        avg7daysPrice: element.avg7daysPrice,
        traderName: element.traderName,
        traderPrice: element.traderPrice,
        traderPriceCur: element.traderPriceCur,
        updated: element.updated,
        slots: element.slots,
        diff24h: element.diff24h,
        diff7days: element.diff7days,
        icon: element.icon,
        img: element.img,
        imgBig: element.imgBig,
        bsgId: element.bsgId,
        reference: element.reference,
      });
      await newItem.save();
    });
    return res.status(200).json("done graping")
  } catch (err) {
    return res.status(500).json(err)
  }
}
module.exports.getAllItems = async (req, res) => {
  const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1 ;
  try {
    const count = await Item.find().countDocuments();
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true: false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
    const items = await Item.find().skip( ( page - 1 ) * pagination ).limit( pagination );
    return res.json( { items: items, count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } );
  } catch (err) {
    return res.status(500).json(err)
  }

};

module.exports.findById = async (req, res) => {
  try {
    const id = req.params.id;
    const result = await Item.findById(id);
    return res.status(200).json({
      item: result,
    });
  } catch (err) {
    return res.status(500).json(err)
  }

};

module.exports.findByName = async (req, res) => {
  try {
    const toSearch = req.params.itemName;
    toSearch.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    const item = await Item.find({ name: new RegExp(toSearch, "gi") });
    if (!item) return res.json({ err: "Not found" });
    return res.status(200).json({item: item});
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
};
