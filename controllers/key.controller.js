const APiKey = require('../models/apikey.model')
const uuidAPIKey = require('uuid-apikey')

module.exports.generateKey = async (req, res) => {
  const name = req.body.name
  let output = uuidAPIKey.create()
  let newKey = new APiKey({
    userName: name,
    KEY: output.apiKey,
  })

  let result = await newKey.save()
  res.json({
    message: 'success',
    'RAIDER-API-KEY': result.KEY,
  })
}
