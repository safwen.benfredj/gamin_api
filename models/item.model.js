const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ItemSchema = new Schema(
  {
    name: String,
    shortName: String,
    price: Number,
    avg24hPrice: Number,
    avg7daysPrice: Number,
    traderName: String,
    traderPrice: Number,
    traderPriceCur: String,
    updated: String,
    slots: Number,
    diff24h: Number,
    diff7days: Number,
    icon: String,
    img: String,
    imgBig: String,
    bsgId: String,
    reference: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Item", ItemSchema);
