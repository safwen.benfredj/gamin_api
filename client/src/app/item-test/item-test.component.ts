import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'OwNgApp-item-test',
  templateUrl: './item-test.component.html',
  styleUrls: ['./item-test.component.scss']
})
export class ItemTestComponent implements OnInit {
  constructor(private itemsService: ItemsService) { }

  items : any

  ngOnInit() {
    this.getAllItems();
  }

  getAllItems(){
    this.itemsService.getAllItems().subscribe(data=>{
      const result: any = data;
      this.items = result.items;
      console.log(this.items)
    })
  }
}
