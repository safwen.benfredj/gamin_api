import { Component } from '@angular/core';

@Component({
  selector: 'OwNgApp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'OwNgApp';
}
