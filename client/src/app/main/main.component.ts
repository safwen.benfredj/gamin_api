import { ItemsService } from './../items.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'OwNgApp-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
 items:any=[];
  constructor(private itmServ:ItemsService) { }

  ngOnInit() {
    this.itmServ.getAllItems().subscribe(data=>{
      let result:any =data;
      this.items=result.items;
      console.log(result);
    })
  }

}
