import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http' ;
import {environment} from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  BASE_URL = environment.BASE_URL;
  constructor(private http:HttpClient) { }

  getAllItems(){
    return this.http.get(this.BASE_URL + "/items/")
  }

  findItemById(id){
    return this.http.get(this.BASE_URL + "/items/find/" + id)
  }

  findItemByName(name){
    return this.http.get(this.BASE_URL + "/items/find/" + name)
  }
}
