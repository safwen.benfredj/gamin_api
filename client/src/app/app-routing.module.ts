import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ItemTestComponent } from './item-test/item-test.component';

const routes: Routes = [
  {
    path:'',
    component:MainComponent
  },
  {
    path:'items',
    component:ItemTestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
