const keyModel = require('../models/apikey.model')

module.exports = async (req, res, next) => {
  const KEY = req.header('RAIDER-API-KEY')
  if (!KEY) {
    return res.status(401).json({ msg: 'Access Denied' })
  }
  try {
    const result = keyModel.findOne({
      KEY: KEY,
    })
    if (result) {
      req.verifiedUser = KEY
      next()
    } else {
      return res.status(500).json({ msg: 'Invalid RAIDER-API-KEY' })
    }
  } catch (err) {
    return res.status(500).json({ msg: 'Invalid RAIDER-API-KEY' })
  }
}
